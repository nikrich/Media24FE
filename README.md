# Tech Assessment
This section defines the rules and guidelines for this tech assessment. The aim of the assessment is to style a single web page from 
the supplied .psd design in the Design/web/psd directory. You have one hour to complete this assessment. Before you start, do the following:

1. Fork this repo and clone the fork
2. Open the index.html and styles.css in your favorite IDE
3. Open index.html directly in browser, no need to host it.
4. Open the design in Design/web/psd directory
5. Style away and request a pull request when done.

Ifffff... you do not have a git account and you want to go the maunual route, do the following:

1. [Download the project](https://gitlab.com/nikrich/Media24FE/repository/master/archive.zip)
2. Open the index.html and styles.css in your favorite IDE
3. Open index.html directly in browser, no need to host it.
4. Open the design in Design/web/psd directory 
5. Style away and mail solution to: [leon.britz@24.com](mailto:leon.britz@24.com)

## Rules
* All the files you need can be found in this repo.
* You have 1 hour to complete the test
* Do not use libraries, use class attributes to style the dom.

## Guidelines
* Don't worry about making it responsive, you won't have enough time in an hour
* Add classes to the html and target them in the CSS
* Open the index.html directly in the browser, no need to host it.


<pre>

    -\-                                                     
    \-- \-                                                  
     \  - -\                                                
      \      \\                                             
       \       \                                            
        \       \\                                              
         \        \\                                            
         \          \\                                        
         \           \\\                                      
          \            \\                                                 
           \            \\                                              
           \. .          \\                                  
            \    .       \\                                 
             \      .    \\                                            
              \       .  \\                                 
              \         . \\                                           
              \            <=)                                         
              \            <==)                                         
              \            <=)                                           
               \           .\\                                           _-
               \         .   \\                                        _-//
               \       .     \\                                     _-_/ /
               \ . . .        \\                                 _--_/ _/
                \              \\                              _- _/ _/
                \               \\                      ___-(O) _/ _/ 
                \                \                  __--  __   /_ /      ***********************************
                \                 \\          ____--__----  /    \_       Best of luck!
                 \                  \\       -------       /   \_  \_     
                  \                   \                  //   // \__ \_   **********************************
                   \                   \\              //   //      \_ \_ 
                    \                   \\          ///   //          \__- 
                    \                -   \\/////////    //            
                    \            -         \_         //              
                    /        -                      //                
                   /     -                       ///                  
                  /   -                       //                      
             __--/                         ///
  __________/                            // |               
//-_________      ___                ////  |                
        ____\__--/                /////    |                
   -----______    -/---________////        |                
     _______/  --/    \                   |                 
   /_________-/       \                   |                 
  //                  \                   /                 
                       \.                 /                 
                       \     .            /                 
                        \       .        /                  
                       \\           .    /                  
                        \                /                  
                        \              __|                  
                        \              ==/                  
                        /              //                   
                        /          .  //                    
                        /   .  .    //                      
                       /.           /                       
                      /            //                       
                      /           /
                     /          //
                    /         //
                 --/         /
                /          //
            ////         //
         ///_________////
</pre>
